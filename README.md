# acmp - CLI tool for managing [acmp](https://acmp.ru/) task solutions.

## Usage

To add a new solution, use `acmp add`:

```bash
$ acmp add <task_id> <file_with_code>
```

To delete an available solution, use `acmp del`:

```bash
$ acmp del <id>
```

To show a list of available task solutions, use `acmp list`:

```bash
$ acmp list
```

To show an available task solution, use `acmp show <id>`:

```bash
$ acmp show <id>
```

## Install

To install, use `go get`:

```bash
$ go get -d gitlab.com/darleneeon/acmp
```

## Author

[darleneeon](https://gitlab.com/darleneeon)
