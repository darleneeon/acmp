package command

import (
	"fmt"
	"strconv"

	"github.com/darleneeon/acmp/model"
)

type ShowCommand struct {
	Meta
}

func (c *ShowCommand) Run(args []string) int {
	if len(args) != 1 {
		fmt.Println(c.Help())
		return 0
	}

	// Get id argument.
	id, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("<id> argument must be a number.")
		return 1
	}

	// Get solution.
	solution, err := model.GetSolution(id)
	if err != nil {
		fmt.Printf("Solution [%d] not found\n", id)
		return 1
	}

	// Show solution
	fmt.Printf("acmp task ID: %d\nCode:\n\n%s\n", solution.AID, solution.Code)

	return 0
}

func (c *ShowCommand) Synopsis() string {
	return "Show an available solution"
}

func (c *ShowCommand) Help() string {
	helpText := `Usage: acmp show <id>
`
	return helpText
}
