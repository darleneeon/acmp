package command

import (
	"fmt"

	"github.com/darleneeon/acmp/model"
)

type ListCommand struct {
	Meta
}

func (c *ListCommand) Run(args []string) int {
	// Get solutions
	solutions, err := model.GetSolutions()
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
		return 1
	}

	for i := 0; i < len(solutions); i++ {
		s := solutions[i]
		fmt.Printf("[%d]\t %6d %s\n", s.ID, s.AID, s.Lang)
	}

	return 0
}

func (c *ListCommand) Synopsis() string {
	return "Show a list of available solutions"
}

func (c *ListCommand) Help() string {
	helpText := `Usage: acmp list
`
	return helpText
}
