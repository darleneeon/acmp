package command

import (
	"fmt"
	"strconv"

	"github.com/darleneeon/acmp/model"
)

type DelCommand struct {
	Meta
}

func (c *DelCommand) Run(args []string) int {
	if len(args) != 1 {
		fmt.Println(c.Help())
		return 0
	}

	// Get id argument.
	id, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("<id> argument must be a number.")
		return 1
	}

	// Check solution.
	_, err = model.GetSolution(id)
	if err != nil {
		fmt.Printf("Solution [%d] not found.\n", id)
	}

	// Delete solution.
	if err = model.DeleteSolution(id); err != nil {
		fmt.Printf("Error during solutinon deleting: %s\n", err.Error())
		return 1
	}

	return 0
}

func (c *DelCommand) Synopsis() string {
	return "Delete an available solution"
}

func (c *DelCommand) Help() string {
	helpText := `Usage: acmp del <id>
`
	return helpText
}
