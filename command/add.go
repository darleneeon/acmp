package command

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/darleneeon/acmp/model"
)

type AddCommand struct {
	Meta
}

func (c *AddCommand) Run(args []string) int {
	if len(args) != 2 {
		fmt.Println(c.Help())
		return 0
	}

	// Get arguments.
	filename := strings.TrimSpace(args[1])
	taskID, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("<task_id> argument must be a number.")
		return 1
	}

	// Read code from file.
	code, err := getCode(filename)
	if err != nil {
		fmt.Println("<file_with_code> argument must be a file.")
		return 1
	}

	// Set solution.Lang
	// Works only with .py and .cpp files.
	var lang string

	switch filepath.Ext(filename) {
	case ".py":
		lang = "Python"
	case ".cpp":
		lang = "C++"
	default:
		lang = "Unknown"
	}

	// Setup solution model.
	solution := model.SolutionModel{
		AID:  taskID,
		Lang: lang,
		Code: code,
	}

	// Save solution.
	if err := model.SaveSolution(&solution); err != nil {
		fmt.Printf("Error during solution saving.\n%s", err.Error())
		return 1
	}

	return 0
}

func (c *AddCommand) Synopsis() string {
	return "Add a new task solution"
}

func (c *AddCommand) Help() string {
	helpText := `Usage: acmp add <task_id> <file_with_code>
`
	return helpText
}

func getCode(filename string) (string, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}

	return string(b), nil
}
