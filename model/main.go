package model

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"github.com/boltdb/bolt"
)

var DB *bolt.DB

// SetupDB returns database instance.
func SetupDB(filename string) {
	var err error

	// Generate path for database file.
	ex, err := os.Executable()
	if err != nil {
		fmt.Printf("Error during database-path generation: %s\n", err.Error())
	}

	exPath := filepath.Dir(ex)
	dbPath := exPath + string(filepath.Separator) + filename

	// Connect
	DB, err = bolt.Open(dbPath, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		fmt.Printf("Error during database connection: %s\n", err.Error())
	}

	// Create required buckets.
	if err = DB.Update(func(tx *bolt.Tx) error {
		_, err = tx.CreateBucketIfNotExists([]byte("solutions"))
		if err != nil {
			panic(err)
		}

		return nil
	}); err != nil {
		panic(err)
	}
}

// ----------------------------------------------------------------------------

// SolutionModel struct represents solution model.
type SolutionModel struct {
	ID   int    // database record ID
	AID  int    // acmp task ID
	Lang string // code language
	Code string
}

// ----------------------------------------------------------------------------

// GetSolutions returns all solutions from database.
func GetSolutions() ([]SolutionModel, error) {
	var solutions []SolutionModel

	err := DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("solutions"))

		// Create slice for solutions.
		cap := b.Sequence()
		solutions = make([]SolutionModel, 0, cap)

		// Get solutions.
		c := b.Cursor()

		for k, data := c.First(); k != nil; k, data = c.Next() {
			solution, err := unmarshal(data)
			if err != nil {
				return err
			}

			solutions = append(solutions, solution)
		}

		return nil
	})

	return solutions, err
}

// GetSolution returns solution from database.
func GetSolution(id int) (*SolutionModel, error) {
	var solution SolutionModel

	err := DB.View(func(tx *bolt.Tx) error {
		var err error

		b := tx.Bucket([]byte("solutions"))

		// Get solution.
		data := b.Get(itob(id))
		solution, err = unmarshal(data)

		return err
	})

	return &solution, err
}

// SaveSolution saves solution in the databse.
func SaveSolution(sm *SolutionModel) error {
	return DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("solutions"))

		// Generate ID for the solution.
		id, _ := b.NextSequence()
		sm.ID = int(id)

		// Marshal issue into bytes.
		buf, err := marshal(sm)
		if err != nil {
			return err
		}

		// Persist bytes to solutions bucket.
		return b.Put(itob(sm.ID), buf)
	})
}

// DeleteSolution delets solution from the database.
func DeleteSolution(id int) error {
	return DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("solutions"))

		return b.Delete(itob(id))
	})
}

// ----------------------------------------------------------------------------

// Marshal solution model into bytes.
func marshal(sm *SolutionModel) ([]byte, error) {
	buf, err := json.Marshal(sm)
	if err != nil {
		return nil, err
	}

	return buf, nil
}

// Unmarshal bytes into solution model.
func unmarshal(data []byte) (SolutionModel, error) {
	var sm SolutionModel

	if err := json.Unmarshal(data, &sm); err != nil {
		return sm, err
	}

	return sm, nil
}

func itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}
