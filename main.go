package main

import (
	"os"

	"github.com/darleneeon/acmp/model"
)

func main() {
	// Setup database
	model.SetupDB("acmp.db")

	// Run program
	os.Exit(Run(os.Args[1:]))
}
